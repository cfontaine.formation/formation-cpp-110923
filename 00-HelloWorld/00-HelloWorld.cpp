#include <iostream>
using namespace std;

/*
	Commentaire
	sur plusieurs
	lignes
*/

// Point d'entrée du programme
int main()
{
	cout << "Hello World!" << endl; // Commentaire fin de ligne
}

// Ctrl K + Ctrl D -> mise en forme automatique du code
// Ctrl K + Ctrl C -> commenter les lignes qui sont sélectionnées
// Ctrl K + Ctrl U -> décommenter les lignes qui sont sélectionnées
