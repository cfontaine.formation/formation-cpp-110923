#include <iostream>
#include <bitset>

using namespace  std;

#define VAL_CONST 42;  // Macro en C => pour définir une constante  (à éviter en C++)

// Variable global -> Variable déclarée directement dans le fichier et pas dans un bloc de code
//				      (à éviter au maximum)
// int vGlobal=123;
// Si elle n'est pas initialisée un variable globale est initialisée à 0
int vGlobal;

int main()
{
	// Déclaration variable
	int i;

	// Intialisation
	i = 42;

	// Déclaration et initialisation 
	double hauteur = 42.5;

	cout << i << "hauteur= " << hauteur << endl;

	int mb, mc = 12;
	mb = 23;
	cout << mb << " " << mc << endl;

	// Déclaration et initialisation
	int j = 10;				// C
	int jCpp98(10);			// C++ 98
	int jCpp11{ 10 };		//C++ 11
	int jCpp11Bis = { 10 };
	cout << j << "  " << jCpp98 << "  " << jCpp11 << "  " << jCpp11Bis << endl;

	// Littéral entier -> par défaut int
	long l = 10L;			// Litéral long -> L
	unsigned int ui = 10U;	// Littéral unsigned -> U
	long long lli = 10LL;	// Littéral long long -> LL en C++ 11

	// Changement de base
	int decimal = 42;		// base 10 par défaut: décimal
	int hexa = 0xFF32;		// base 16 héxadécimal -> 0x
	int octal = 0342;		// base 8  octal -> 0
	int bin = 0b110101;		// base 2  binaire -> 0b en C++14
	cout << decimal << " " << hexa << " " << octal << " " << bin << endl;

	// Littéral virgule flottante
	double d1 = 1.5;	//	5.	-> OK .78 -> OK
	double d2 = 1.230e3;

	// Littéral virgule flottante -> par défaut de type double
	float f = 1.2F;			// littéral float -> F
	long double ld = 1.23L;	// Littéral long double -> L

	// Littéral booléen
	bool test = true; // false

	// Littéral caractère
	char c = 'a';
	char cHexa = '\x41';	// caractère en hexadécimal
	char cOctal = '\32';	// caractère en octal

	// Variable globale
	cout << "Variable globale" << vGlobal << endl; // 0

	// On déclare une variable locale qui a le même le nom que la variable globale
	int vGlobal = 23;
	// La variable globale est masquée par la variable locale
	cout << "Variable locale" << vGlobal << endl; // 23
	// Pour accéder à la variable globale, on utilise l’opérateur de résolution de portée ::
	cout << "Variable globale" << ::vGlobal << endl; //0

	// En C pour une constante, on utilise #define 
	int v = VAL_CONST;
	cout << v << endl;

	// En C++ -> const
	// Constante
	const double PI = 3.14;
	// PI = 3.1419; // Erreur: on ne peut pas modifier une constante

	// Constexpr C++ 11
	constexpr double r = 3.14;
	constexpr double pi2 = r * 2.0;
	cout << pi2 << endl;

	// Typage implicite C++ 11 -> auto
	// Le type de la variable est définie à partir de l'expression d'initialisation
	auto implicite = false; // implicite de type bool
	auto implicite2 = 123.4f; // implicite de type float
	// Attention aux chaine de caratères: avec auto le type sera const char* et pas std::string
	auto impliciteStr = "azerty"; // le type const char *

	auto p = PI; // auto ne tient pas compte de const et constexpr
	p = 5.9; // p n'est constant, on peut le modifier

	// decltype  C++ 11 -> déclarer qu'une variable est de même type qu'une autre
	decltype(c) c2 = 'z';	// c2 est de type char
	cout << c2 << endl;

	decltype(PI) PI3 = 3.1419;	// p3  est  type double, Comme Pi est une constante p3 est ausi une constante
	//PI3 = 5;	// => erreur PI3 constant
	decltype(3 * PI + 12.5) p4 = 11.3; // p4 est de type double 

	// Opérateur
	// Opérateur arithmétique
	int aa = 1;
	int bb = 3;
	int res = aa + bb;
	cout << res << " " << bb % 2 << endl;	// % => modulo (reste de division entière) uniquement avec des entiers positif

	// Pré-incrémentation
	int inc = 0;
	res = ++inc; // inc=1 res=1
	cout << inc << " " << res << endl;

	// Post-incrémentation
	inc = 0;
	res = inc++; // res=0 inc=1
	cout << inc << " " << res << endl;

	// Affectation composée
	res = 12;
	res += 10; // correspond à res=res+10

	int b4 = 0b101011;
	b4 >>= 2; // correspond à  b4 = b4>>2;
	cout << bitset<32>(b4) << endl; //1010

	// Opérateur de comparaison
	int val;
	cin >> val;
	bool tst1 = val > 10;	// Une comparaison a pour résultat un booléen
	cout << tst1 << endl;

	// Opérateur logique
	//  non !
	bool inv = !tst1;
	cout << inv << endl;

	// T1 T2 | ET  OU Ou exclusif
	// F  F  | F   F	F
	// F  T  | F   T	T
	// T  F  | F   T	T
	// T  T  | T   T	F

	// Opérateur court-circuit && et || (évaluation garantie de gauche à droite)
	// && dés qu'une comparaison est fausse, les suivantes ne sont pas évaluées
	// || dés qu'une comparaison est vrai, les suivantes ne sont pas évaluées
	bool tst2 = val > 100 && val++ != 500;	// si val > 100 est faux,  val++ != 500 n'est pas évalué
	cout << tst2 << " " << val << endl;
	bool tst3 = val > 100 || val++ < 5;		// si val > 100 est vrai,  val<5 n'est pas évalué
	cout << tst3 << " " << val << endl;

	// Opérateur binaire (uniquement avec des entiers)
	// Pour les opérations sur des binaires, on peut utiliser les entiers non signé
	unsigned int b1 = 0b101011;
	cout << ~b1 << " " << bitset<32>(~b1) << endl;		// Complémént: 1 -> 0 et 0 -> 1
	cout << bitset<32>(b1 & 0b1000) << endl;			// Et bit à bit				1000
	cout << bitset<32>(b1 | 0b10100) << endl;			// Ou bit à bit				11111
	cout << bitset<32>(b1 ^ 0b11100) << endl;			// Ou exclusif bit à bit	110111

	// Opérateur de décallage
	cout << bitset<32>(b1 >> 2) << endl; // 1010	Décalage à droite de 2
	cout << bitset<32>(b1 << 1) << endl; // 1010110	Décalage à gauche de 1 (-> équivaut à une multiplication par 2)
	cout << bitset<32>(b1 >> 1) << endl; // 10101	Décalage à droite de 1 (-> équivaut à une division par 2)

	// sizeof
	cout << sizeof(res) << endl;	// nombre d'octets de la variable res -> 4 
	cout << sizeof(double) << endl; // nombre d'octets du type double -> 8

	// Opérateur séquentiel, toujours évalué de gauche->droite
	inc = 0;
	int o = 12;
	res = (inc++, o + 10); // res=o+10; inc=inc+1
	cout << res << "" << inc << endl; // res=22 inc=1

	// Conversion implicite -> rang inférieur vers un rang supérieur (pas de perte de donnée)
	int convI = 13;
	double convD = 12.3;
	double convRes = convI + convD; // 13.0+12.3 =25.3
	cout << convRes << endl;

	// Promotion numérique -> short, bool ou char dans une expression -> convertie automatiquement en int
	short s1 = 2;
	short s2 = 3;
	int s3 = s1 + s2;

	// boolean vers entier
	int convI1 = false; // false ->  0
	int convI2 = true;	// true -> 1
	cout << convI1 << "  " << convI2 << endl;

	// entier,nombre à virgule flottante et pointeur -> boolean
	bool cb1 = 34;		// 34 différent de 0 -> true
	bool cb2 = 0;		// égale à 0 -> false
	int* ptr = nullptr;
	bool cb3 = ptr;		// pointeur = à null -> false
	ptr = &convI1;
	bool cb4 = ptr;		// pointeur différent de null -> true
	cout << cb1 << " " << cb2 << " " << cb3 << " " << cb4 << endl;

	// Conversion explicite -> opérateur de cast
	int te = 11;
	int div = 2;
	double ted = te / div;
	cout << ted << endl; // 5

	// en C -> opérateur de cast
	double teC = ((double)te) / div;
	cout << teC << endl; // 5.5

	// en C++ -> avec static_cast, le compilateur fait plus de vérification (sur le type)
	double teCpp = static_cast<double>(te) / div; // 5.5
	cout << teCpp << endl; // 5.5

	// Opérateur d'affectation conversion systémathique (implicite ou explicite)
	// conversion explicite: double -> int
	double ta = 3.5;
	int convExp = ta;
	cout << convExp << endl;	// 3

	// conversion implicite: int->double
	int ta2 = 4;
	double convImp = ta2;
	cout << convImp << endl;	//4.0


}