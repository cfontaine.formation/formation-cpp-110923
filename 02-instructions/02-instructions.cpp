#include <iostream>
using namespace std;

int main()
{
	// Exercice: Somme
	// Saisir 2 nombres décimaux et afficher le résultat dans la console sous la forme 1.5 + 3.6 = 5.1
	double a, b;
	cout << "Saisir 2 nombres decimaux: ";
	cin >> a >> b;
	double res = a + b;
	cout << a << " + " << b << " = " << res;

	// Exercice: Moyenne
	// Saisir 2 nombres entiers et afficher la moyenne dans la console
	int v1, v2;
	cout << "Saisir 2 nombres entiers: ";
	cin >> v1 >> v2;
	//double moyenne = ((double) (v1 + v2)) / 2;
	//double moyenne = static_cast<double>(v1 + v2) / 2;
	double moyenne = (v1 + v2) / 2.0;
	cout << "moyenne=" << moyenne << endl;

	// Condition if
	int val;
	cin >> val;
	if (val > 10)
	{
		cout << "La valeur est > a 10";
	}
	else if (val == 10)
	{
		cout << "La valeur est  = a 10";
	}
	else {
		cout << "La valeur est < a 10";
	}

	// Exercice: Trie de 2 Valeurs
	//Saisir 2 nombres et afficher ces nombres triés dans l'ordre croissant sous la forme 1.5 < 10.5
	cout << "Saisir 2 nombres entiers: ";
	int va, vb;
	cin >> va >> vb;
	if (va > vb) {
		cout << vb << "<" << va;
	}
	else {
		cout << va << "<" << vb;
	}

	// Exercice: Interval
	// Saisir un nombre et dire s'il fait parti de l'intervalle - 4 (exclus)et 7 (inclus)
	int v;
	cout << "Saisir un nombre entier: ";
	cin >> v;
	if (v > -4 && v <= 7) {
		cout << v << " fait parti de l'interval" << endl;
	}

	// Condition switch
	const int LUNDI = 1;
	int jours;
	std::cout << "Saisir un nombre entre 1 et 7 ";
	cin >> jours;
	switch (jours)
	{
	case LUNDI:
		std::cout << "Lundi" << std::endl;
		break;
	case LUNDI + 5:
	case LUNDI + 6:
		std::cout << "Week end !" << std::endl;
		break;
	default:
		std::cout << "Un autre jour" << std::endl;
	}

	// Exercice: Calculatrice
	// Faire un programme calculatrice
	//	Saisir dans la console
	//	- un double
	//	- une caractère opérateur qui a pour valeur valide : + - * /
	//	- un double

	// Afficher:
	// - Le résultat de l’opération
	// - Une message d’erreur si l’opérateur est incorrecte
	// - Une message d’erreur si l’on fait une division par 0

	double d1, d2;
	char op;
	cout << "Saisir un nombre ,un operateur et un nombre" << endl;
	cin >> d1 >> op >> d2;
	switch (op) {
	case '+':
		cout << d1 << " + " << d2 << " =  " << (d1 + d2) << endl;
		break;
	case '-':
		cout << d1 << " - " << d2 << " =  " << (d1 - d2) << endl;
		break;
	case '*':
		cout << d1 << " * " << d2 << " =  " << (d1 * d2) << endl;
		break;
	case '/':
		if (d2 == 0.0) { //(d2>-1.0e-10 && d2<1.0e-10)
			cout << "Division par 0" << endl;
		}
		else {
			cout << d1 << " / " << d2 << " =  " << (d1 / d2) << endl;
		}
		break;
	default:
		cout << op << " n'est pas un operateur" << endl;
	}

	// Opérateur ternaire -> utiliser pour faire une allocation conditionnelle
	double valeur;
	cout << "Saisir un nombre: ";
	cin >> valeur;
	double abs = valeur > 0.0 ? valeur : -valeur;

	// idem avec un if
	//double abs;
	//if (valeur > 0.0) {
	//	abs = valeur;
	//}
	//else {
	//	abs = -valeur;
	//}
	cout << "|" << valeur << "|=" << abs;

	// Boucle
	// Boucle :while
	int k = 0;
	while (k < 10) {
		cout << k << endl;
		k++;
	}

	int kc = 0;
	while (kc < 10) {
		cout << kc << endl;
		cin >> kc;
	}

	// Boucle :do while
	k = 0;
	do {
		cout << k << endl;
		k++;
	} while (k < 10);

	// Boucle :for
	for (int i = 100; i >= 0; i = i - 10) {
		cout << "i=" << i << endl;
	}

	// Intructions de branchement
	// break
	for (int i = 0; i < 10; i++) {
		cout << "i=" << i << endl;
		if (i == 2) {
			break; // break -> termine la boucle
		}
	}

	// continue
	for (int i = 0; i < 10; i++) {
		if (i == 2) {
			continue; // continue -> on passe à l'itération suivante
		}
		cout << "i=" << i << endl;
	}

	// goto
	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 5; j++) {
			cout << "i=" << i << "j" << j << endl;
			if (i == 3) {
				goto EXIT_LOOP; // Utilisation de goto pour sortir de 2 boucles imbriquée
			}
		}
	}
EXIT_LOOP:

	// boucle infinie
	//while (true) {

	//}
	//for (;;) {

	//}

	// Exercice: Table de multiplication
	// Faire un programme qui affiche la table de multiplication pour un nombre entre 1 et 9
	//	1 X 4 = 4
	//	2 X 4 = 8
	//	…
	//	9 x 4 = 36
	//	Si le nombre passé en paramètre est en dehors de l’intervalle 1 à 9, on arrête sinon on redemande une nouvelle valeur
	while (true) {
		int m;
		cin >> m;
		if (m < 1 || m>9) {
			break;
		}
		for (int mul = 1; mul < 10; mul++) {
			cout << mul << " x " << m << " = " << m * mul << endl;
		}
	}

	// Exercice: Quadrillage
	// Créer un quadrillage dynamiquement on saisit le nombre de colonne et le nombre de ligne
	//	ex : pour 2 3

	//	[][]
	//	[][]
	//	[][]

	int col;
	int row;
	cin >> col >> row;
	for (int l = 0; l < row; l++) {
		for (int c = 0; c < col; c++) {
			cout << "[ ]";
		}
		cout << endl;
	}

}