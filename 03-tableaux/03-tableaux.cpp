#include <iostream>
using namespace std;

// C++ 98
enum Direction { NORD = 90, SUD = 270, EST = 0, OUEST = 180 };

enum Emballage { CARTON = 90, PLASTIQUE = 270, SANS = 0, PAPIER = 180 };

// En C++11 -> enum class 
enum class Motorisation {ESSENCE=95,DIESEL=2,GPL=10,ELECTRIQUE=3};

int main()
{
	// Tableau (statique) à une dimension
	// Déclaration: type nom[taille]
	double t[5];

	// Initialisation du tableau
	for (int i = 0; i < 5; i++) {
		t[i] = 0.0;
	}

	// Accès à un élément du tableau
	t[0] = 3.5;
	cout << t[0] << endl;

	// Parcourir un tableau
	for (int i = 0; i < 5; i++) {
		cout << "t[" << i << "]=" << t[i] << endl;
	}

	// Parcourir un tableau en c+11
	// uniquement en lecture -> la modification de la variable elm n' a d'influence sur le contenu du tableau
	for (auto elm : t) { //double
		cout << elm << endl;
		elm = 1.23;
	}

	// En utilisant une référence -> la modifcation de la variable va modifier le contenu du tableau
	for (auto& elm : t) { //double
		cout << elm << endl;
		elm = 1.23;
	}

	for (auto elm : t) {
		cout << elm << endl;
	}

	// Déclaration et initialisation d'un tableau
	int t2[] = { 2,6,-7,3 };		// la taille du tableau correspond au nombre d'élément
	// int t2[4] = {1,2};			// correspont à {1, 2, 0, 0}
	// int t2[4] = {};				// initialiser à la valeur 0 correspond à {0,0,0,0}
	// int t2[4] = { 2,6,-7,3,6 };	// erreur -> trop de valeurs
	for (auto e : t2) {
		cout << e << endl;
	}

	// Calculer la taille d'un tableau (ne fonctionne pas avec les tableaux passés en paramètre de fonction
	cout << "taille du tableau=" << sizeof(t2) / sizeof(t2[0]) << endl;

	for (int i = 0; i < sizeof(t) / sizeof(t[0]); i++) {
		t[i] = 110.0;
	}
	for (auto e : t) {
		cout << e << endl;
	}

	// la taille du tableau doit être une constante ou une litterale
	const int S = 4;
	int t3[S];
	//int t3[4];

	// Exercice : tableau
	// Trouver la valeur maximale et la moyenne d’un tableau de 5 entiers: -7, 4, 8, 0, -3
	int tab[] = { -7, -4, -8, -2, -3 };
	int max = tab[0];
	//  int somme = 0;
	double somme = 0.0;
	for (auto e : tab) {
		if (e > max) {
			max = e;
		}
		somme += e;
	}
	// cout << "maximum=" << max <<" moyenne="<<static_cast<double>(somme) / 5 << endl;
	cout << "maximum=" << max << " moyenne=" << somme / 5 << endl;

	// Tableau à 2 dimensions
	// Déclaration
	char t2d[3][2];

	// Initialisation
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 2; j++) {
			t2d[i][j] = '-';
		}
	}

	// Déclaration est initialistion
	// char t2d[3][2] = {}; // initialisé à zéro
	// char t2d[3][2] = { {'a','z'},{'e','r'},{'r','t'} };
	char t2d[3][2] = { 'a','z','e','r','r','t' };

	// Accès à un élément
	t2d[0][1] = 'a';
	cout << t2d[0][1] << endl;

	// Parcourir un tableu à 2 dimmension
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 2; j++) {
			cout << t2d[i][j] << '\t';
		}
		cout << endl; //'\n' équivalent endl
	}

	// Calculer le nombre  de colonnne  d'un tableau (ne fonctionne pas avec les tableaux passés en paramètre)
	int nbColonne = sizeof(t2d[0]) / sizeof(t2d[0][0]);	// 2
	cout << nbColonne << endl;

	// Calculer le nombre  de ligne  d'un tableau
	int nbLigne = sizeof(t2d) / sizeof(t2d[0]);	// 3
	cout << nbLigne << endl;

	// Calculer le nombre  d'élément  d'un tableau
	int nbElement = sizeof(t2d) / sizeof(t2d[0][0]);	// 6
	cout << nbElement << endl;

	// Pointeur
	// Déclaration d'un pointeur de double
	double* ptr; // pointeur -> une variable qui contient une adresse mémoire

	double v = 12.34;

	// &v -> adresse de la variable v
	cout << v << " adresse= " << &v << endl;
	ptr = &v;
	cout << " ptr=" << ptr << endl;

	// *ptr -> Accès au contenu de la variable pointer par ptr
	cout << *ptr << endl;	// 12.34
	*ptr = 34.56;
	cout << v << endl;		// 34.56

	// Un pointeur qui ne pointe sur rien
	double* ptr2 = 0; // 0 -> en C++ (NULL -> en C)
	double* ptr3 = nullptr; // en C++ 11

	// On peut affecter un pointeur avec un pointeur du même type
	ptr2 = ptr;		// ptr2 contient aussi l'adresse de val
	cout << ptr << " " << ptr2 << endl;
	cout << *ptr << " " << *ptr2 << endl;

	// On peut comparer 2 pointeurs du même type  (== ou !=)
	if (ptr == ptr3) {
		cout << "ptr == ptr3" << endl;
	}
	else {
		cout << "ptr != ptr3" << endl;
	}

	// ou comparer un pointeur à 0 ou nullptr
	if (ptr2 != nullptr) {
		cout << "ptr2 different de null" << endl;
	}

	// Pointeur constant
	// En préfixant un pointeur avec const -> Le contenu de la variable pointée est constant
	const double* ptrCst = &v;
	cout << *ptrCst << endl;
	// *ptrCst = 45.6; // on ne peut plus modifier le contenu pointé par l'intermédiaire du pointeur
	ptrCst = nullptr;  // mais on peut modifier le pointeur

	// En postfixant un pointeur avec const -> le pointeur est constant
	double* const ptrCstPost = &v;
	cout << *ptrCstPost << endl;
	*ptrCstPost = 90.0;		// On peut modifier le contenu pointé
	//ptrCstPost = nullptr;	// Le pointeur ptrCstPost est constant, on ne peut plus le modifier

	// En préfixant et en postfixant un pointeur avec const: le pointeur ptrCst2 est constant et
	// l'on ne peut plus modifier le contenu de la variable pointée par l'intermédiaire du pointeur
	const double* const ptrCst2 = &v;
	cout << *ptrCstPost << endl;
	//*ptrCst2 = 90.0;
	// ptrCst2 = nullptr;

	// affecté un pointeur constant avec un pointeur => OK
	const double* ptr4 = ptr;

	// affecté un pointeur  avec un pointeur constant => erreur
	//double* ptr5 = ptrCst;

	// en C, opérateur de cast permet de supprimer le qualificatif const
	double* ptr5 = (double*)ptrCstPost;
	*ptr5 = 5.6;
	cout << *ptr5 << endl;

	// en C++, const_cast permet de supprimer le qualificatif const
	double* ptr6 = const_cast<double*>(ptrCstPost);
	*ptr6 = 5.7;
	cout << *ptr6 << endl;

	// Exercice: Pointeur
	// Créer
	// - 3 variables de type double: v1, v2, v3
	// - créer un pointeur de double : ptrChoix
	//	entrer une valeur entre 1 et 3 et  Afficher la valeur de la variable choisie par l'intermédiare du pointeur
	double v1 = 1.3;
	double v2 = 4.5;
	double v3 = 7.8;
	double* ptrChoix = nullptr;

	char choix;
	cin >> choix;
	switch (choix) {
	case '1':
		ptrChoix = &v1;
		break;
	case '2':
		ptrChoix = &v2;
		break;
	case '3':
		ptrChoix = &v3;
	}
	if (ptrChoix != nullptr) {
		cout << *ptrChoix << endl;
	}


	// Tableau et pointeur
	int tp[] = { 3,7,-5,9 };
	// nom du tableau est un pointeur sur le 1er élément de tableau
	cout << tp << endl;		// pointeur sur le 1er élément du tableau
	cout << *tp << endl;	// contenu du 1er élément deutableau

	// On accède au 4ème élément du tableau
	cout << tp + 3 << endl;
	cout << *(tp + 3) << endl;

	int* ptrTab = tp;
	cout << *(ptrTab + 2) << " " << ptrTab[2] << endl;

	// Arithmétique des pointeurs
	cout << *ptrTab << endl;//3
	ptrTab = ptrTab + 2;
	cout << *ptrTab << endl; //-5
	ptrTab++;
	cout << *ptrTab << endl; //9

	// Exercice: Pointeur (tableau)
	// idem en remplaçant les variables v1 à v3 par un tableau de double de 5 éléments
	double td[] = { 7.5, -4.6, -8.2, 2.0, -3.1 };
	int choix2;
	cin >> choix2;
	ptrChoix = td;
	if (choix2 > 0 && choix2 < 6) {
		cout << *(ptrChoix + choix2 - 1) << endl;
	}

	// Pointeur void -> C
	double d = 123.4;
	double* ptrD = &d;
	void* ptrVoid = ptrD;
	//cout << *ptrVoid << endl;	// *void -> ne permet pas le déférencement
	int* ptrI = (int*)ptrVoid;
	cout << *ptrI << endl;

	// reinterpret_cast en c++
	int rti = 0x61 | 0x64 << 8 | 0x55 << 16; // 00 55 64 61
	cout << hex << rti << endl;
	int* ptrRti = &rti;

	char* ptrChr = reinterpret_cast<char*>(ptrRti);
	cout << *ptrChr << " " << static_cast<int>(*ptrChr) << endl; // 61
	ptrChr++;
	cout << *ptrChr << " " << static_cast<int>(*ptrChr) << endl; //64
	cout << *(ptrChr + 1) << " " << static_cast<int>(*(ptrChr + 1)) << dec << endl; //55

	// Allocation dynamique de mémoire
	int* ptrDyn = new int;	// new => allocation de la mémoire
	*ptrDyn = 42;
	cout << ptrDyn << "  " << *ptrDyn << endl;
	delete ptrDyn;			// delete => libération de la mémoire
	// cout << ptrDyn << endl;
	ptrDyn = nullptr;

	// Pas de problème, si on utilise delete sur un pointeur null, il ne  se passe rie
	delete ptrDyn;

	// Allocation dynamique avec initialisation C++
	ptrDyn = new int(3);
	cout << *ptrDyn << endl;
	delete ptrDyn;

	// Allocation dynamique avec initialisation C++ 11
	ptrDyn = new int{ 3 };
	cout << *ptrDyn << endl;
	delete ptrDyn;

	// Allocation dynamique d'un tableau
	int size = 5;
	double* ptrTabDyn = new double[size];	// new [] => création d'un tableau dynamique
	*ptrTabDyn = 4.2;						// équivalant à ptrTabDyn[0]=4.2
	*(ptrTabDyn + 1) = 3.14;				// équivalant à ptrTabDyn[1] = 3.14
	// On peut accèder à un tableau dynamique comme à un tableau statique
	ptrTabDyn[2] = 5.6;
	ptrTabDyn[3] = 3.6;
	ptrTabDyn[4] = 7.6;

	for (int i = 0; i < size; i++) {
		cout << ptrTabDyn[i] << endl;
	}

	delete[] ptrTabDyn;		// delete [] => libération d'un tableau dynamique
	ptrTabDyn = nullptr;

	// Exercice : Tableau dynamique
	// Modifier le programme Tableau pour faire la saisie :
	// - de la taille du tableau
	// - des éléments du tableau
	// Trouver la valeur maximale et la moyenne du tablea
	int sizeTab;
	cin >> sizeTab;
	int* tabDyn = new int[sizeTab];
	for (int i = 0; i < sizeTab; i++) {
		cout << "tab[" << i << "]=";
		cin >> tabDyn[i];
	}
	int maxVal = tabDyn[0];
	double sommeTab = 0.0;
	for (int i = 0; i < sizeTab; i++) {
		if (tabDyn[i] > maxVal) {
			maxVal = tabDyn[i];
		}
		sommeTab += tabDyn[i];
	}
	cout << "Moyenne=" << sommeTab / sizeTab << " Maximum" << maxVal << endl;
	delete[] tabDyn;
	
	// Référence
	// Une référence coorrpond à un autre nom ue l'on donne à la variable
	// Pour initialiser une référence on utilse un lvalue (=qui possède une adresse) 
	int u = 2;
	int& ref = u; // ou auto
	cout << u << " " << ref << endl;
	ref = 4;
	cout << u << " " << ref << endl;

	//double &refD; // Obligatoirement l'initialiser avec une variable
	//double &refD2 = 34; // Obligatoirement  une variable

	// référence constante
	const int& refCst = u;
	u = 45;
	cout << u << " " << refCst << endl;
	// refCst = 3;

	// une référence constante peut être initialisée avec une littérale
	const double& refCst2 = 42.0;
	cout << refCst2 << endl;
	//refCst2 = 3.14;

	// énumération
	Direction dir = Direction::NORD;	//NORD;
	if (dir == Direction::NORD) {
		cout << "Nord" << endl;
	}

	// Convertion implicite énumération -> int
	int valDir = dir; // 0
	cout << valDir << endl;

	// Convertion explicite  int -> énumération (à éviter)
	valDir = 120;
	dir = (Direction)valDir;
	cout << dir << endl;

	// pas de vérification de type enum de C++98
	dir = SUD;
	Emballage emb = PLASTIQUE;
	if (dir == emb) {
		cout << "==" << endl;
	}

	// En C++ 11
	Motorisation m = Motorisation::ESSENCE;

	// en C+11 il n'y plus convertion implicte
	int valM = static_cast<int>(m); // convertion explicite
	cout << valM << endl;

	// enum class il ya une vérificcation du type
	Direction dir1 = Direction::SUD;
	//if (m == dir1) {
	//	cout << "==" << endl;
	//}

	switch (m) {
	case Motorisation::ESSENCE:
		cout << "Essence" << endl;
		break;
	case Motorisation::DIESEL:
		cout << "Diesel" << endl;
		break;
	default:
		cout << "Autre" << endl;
	}
}

