#include "MesFonctions.h"
#include "MesStructures.h"
#include <iostream>
#include<string>

using namespace std;

// Déclaration des fonctions => déplacer dans le fichier .h d'en-tête
//double multiplication(double, double);
//void afficher(double);

const int TEST_VAR_EXTERN = 456;

// avec static la variable n'est visible que dans ce fichier
static double pi = 3.14;

int main(int argc, char* argv[]) {
	double e = 12.0;
	/*
	// Appel de methode
	double res = multiplication(e, 3.0) + 4.0;
	cout << res << endl;
	cout << multiplication(3.0, 2.0) << endl;

	// Appel de methode (sans retour)
	afficher(3.4);
	afficher(multiplication(3.0, 2.0));

	// Exercice Fonction maximum
	int a;
	int b;
	cin >> a >> b;
	cout << maximum(a, b) << endl;

	// Exercice Fonction paire
	cout << even(4) << endl; // 1
	cout << even(3) << endl; //0

	// Paramètre passé par valeur (par défaut)
	// C'est une copie de la valeur du paramètre qui est transmise à la méthode
	int v = 23;
	testParamValeur(v);
	cout << v << endl; // 23
	testParamValeur(45);
	cout << v << endl; // 23

	// Paramètre passé par adresse
	// La valeur de la variable passée en paramètre est modifiée, si elle est modifiée dans la méthode
	testParamAdresse(&v);	// on passe en paramètre l'adresse de la variable v
	cout << v << endl; //100

	// Les tableaux peuvent être passé par adresse et pas par valeur
	int tab[] = { 2,5,2,6 };
	afficherTab(tab, 4);	// le nom du tableau correspond à un pointeur sur le premier élément du tableau

	// Passage de paramètre par référence
	testParamReference(v);
	cout << v << endl; //300
	// testParamReference(45);

	// Passage de paramètre par référence constante
	testParamReferenceConst(v);
	testParamReferenceConst(45);	// Avec une référence constante, on peut aussi passer des littérals

	// Paramètre par défaut (uniquement pour les paramètres passés par valeur et référence constante)
	testParamDefault(1);			// 1 1 a
	testParamDefault(1, false);		// 1 0 a
	testParamDefault(1, false, 'e');	// 1 0 e

	// Fonction inline
	double d1 = 3.0;
	double res2 = doubler(d1); // double res2=d1*2.0;

	// Surcharge de fonction
	cout << somme(1, 3) << endl;
	cout << somme(1.6, 3.6) << endl;
	cout << somme(1, 3.6) << endl;
	cout << somme(1, 3, 6) << endl;

	// pas correpondence direct des paramètres, le compilateur va faire des conversions des paramètres
	//  pour trouver une fonction qui correspond 
	cout << somme(1, 3.4f) << endl;		// Convertion du float en double
	cout << somme('A', 'a') << endl;	// Convertion des caractères en entier

	// Les arguments double& et const double& sont considérés comme différents
	int gt = 23;
	cout << somme(&gt, 6) << endl;
	cout << somme(3.0, 1) << endl;
	double d = 3.4;
	cout << somme(d, 1) << endl;

	// Fonction récurssive
	int r1 = factorial(3);
	cout << r1 << endl;

	// Paramètre de la fonction main
	for (int i = 0; i < argc; i++) {
		cout << argv[i] << endl;
	}

	// Pointeur de fonction
	ptrf pf = multiplication;
	cout << (*pf)(1.2, 1.3) << endl;
	cout << pf(2.3, 4.5) << endl;
	double p1, p2;
	cin >> p1 >> p2;
	cout << pf(p1, p2) << endl;

	cout << calcul(p1, p2, somme) << endl;
	cout << calcul(p1, p2, multiplication) << endl;

	// Classe de mémorisation static
	testMemStatic();
	testMemStatic();
	testMemStatic();

	// extern
	cout << TEST_VAR_EXTERN << endl;
	testMemExtern();

	// /!\ une fonction ne doit pas retourner une référence ou un pointeur sur une variable locale à une fonction
	string* str = testRetour(4);
	cout << str << " " << *str << endl;
	delete str;

	// Fonction à nombre variable de paramètre  
	// Hérité du C => Problème pas de vérification de type
	cout << moyenne(3, 1, 6, 3) << endl;
	cout << moyenne(1, 6) << endl;
	cout << moyenne(0) << endl;

	// C++ 11 => initializer_list
	cout << moyenneCpp11({}) << endl;
	cout << moyenneCpp11({ 2,5,6,5 }) << endl;

	int* tableau = createTab(4, 6);
	afficherTab(tableau, 4);
	delete[] tableau;

	// Pointeur de pointeur
	tableau = nullptr;
	createTab2(&tableau, 4, 5);
	cout << tableau << endl;
	afficherTab(tableau, 4);
	delete[] tableau;

	// Exercice tableau
	menu();
	*/
	// Structure
	//struct Contact c1; // en C
	Contact c1; //en C++

	//accèder à un champs l'opérateur .
	c1.prenom = "John";
	c1.nom = "Doe";
	c1.email = "jd@dawan.com";
	c1.age = 37;
	//cin >> c1.prenom;
	cout << c1.prenom << endl;
	c1.afficher();

	// Déclaration et initialisation d'une structure
	Contact c2 = { "Jane","Doe","jdoe@dawan.fr",34 };
	c2.afficher();

	// 
	Contact c3 = c2;
	c3.afficher();
	c2.age = 23;
	c2.afficher();
	c3.afficher();

	// on test l'égalité de 2 strucutures -> champs par champs
	bool test = c1.age == c2.age && c1.prenom == c2.prenom && c1.nom == c2.nom && c1.email == c2.email;
	cout << test << endl;

	// Allouer dynamiquement une structure
	Contact* ptrC = new Contact;
	(*ptrC).prenom = "Alan";
	ptrC->nom = "Smithee"; // Opérateur fléche
	ptrC->age = 45;
	ptrC->email = "smithee@dawan.com";
	ptrC->afficher();
	delete ptrC;

	const Contact cCst= { "Marcel","Doe","jdoe@dawan.fr",34 };
	cout << cCst.age << endl;
	// cCst.nom = "Durant";
	cCst.age = 56;

	// Chaine de caractère en C
	// en C, un chaine de caractère est un tableau de caractère qui est terminé par un caractère terminateur '\0'
	// Une chaine constante -> const char*
	const char* strCstC = "Hello";
	//strCstC[0] = 'h';	// => erreur
	cout << strCstC << endl;

	// Chaine de caractère
	char  strC[] = "Hello";
	strC[0] = 'h';
	// Nombre de caractères de la chaine +1 pour le terminateur \0
	cout << strC <<" "<<sizeof(strC)<<endl;

	// Chaine de caractère en C++ -> string
	string strCpp = "Hello";
	cout << strCpp << endl;
	string str1 = string("bonjour");
	string str2 = string(15,'a');
	cout << str1 << " " << str2 << endl;

	// Allouer dynamiquement 
	string* str3 = new string("azerty");
	cout << *str3 << endl;
	delete str3;

	// Concaténation
	strCpp += " World";			// Opérateur + -> 	// Concaténation
	cout << strCpp << endl;
	strCpp.append(" !!!");		// append->permet d'ajouter la chaine passée en paramètre en fin de chaine
	cout << strCpp << endl;
	strCpp.push_back('?');		// push_back -> ajoute le caractère passé en paramètre en fin de chaine
	cout << strCpp << endl;

	// Comparaison
	// On peut utiliser les opérateurs de comparaison avec les chaines de caractères
	if (str1 == str2) {
		cout << "==" << endl;
	}
	else {
		cout << "!=" << endl;
	}

	if (str2 < str1) {
		cout << "Inferieur" << endl;
	}

	// Accès à un cararactère de la chaine [] ou at
	cout << strCpp[1] << endl;
	cout << strCpp.at(1) << endl;

//	cout << strCpp[40] << endl;		// [] -> erreur du programme
//	cout << strCpp.at(40) << endl;	// at -> une exception est lancé

	// length -> nombre de caractères de la chaine (idem pour size)
	cout << strCpp.length() << endl;
	// empty -> retourne true si la chaine est vide
	cout << strCpp.empty() << endl;

	// Extraire une sous-chaine -> substr
	cout << strCpp.substr(5) << endl;		// retourne une  sous-chaine qui commence à l'indice 5 et jusqu'à la fin
	cout << strCpp.substr(6, 5) << endl;	// retourne une  sous-chaine de 5 caractères qui commence à l'indice 6

	// Insérer une sous-chaine -> insert
	strCpp.insert(5, "-----");	// insère la chaine passée en paramètre à la position 5
	cout << strCpp << endl;

	// remplacer une partie de la chaine par une sous-chaine -> replace
	strCpp.replace(6, 1, "_?!");  // remplace à la position 6 , 1 caractère par la chaine de caractère passé en paramètre par _?!
	cout << strCpp << endl;

	// Supprimer une partie de la chaine -> erase
	strCpp.erase(5, 7);		// supprimer 7 caractères de la chaine à partir de la position 5
	cout << strCpp << endl;
	strCpp.erase(12);		// supprimer les caractères de la chaine à partir de la position 12 jusqu'à la fin de la chaine
	cout << strCpp << endl;

	// Recherche de la position de la chaine ou du caractère passée en paramètre dans la chaine str
	// -> find
	cout << strCpp.find('o') << endl;		// à partir du début de la chaine
	cout << strCpp.find('o', 5) << endl;		// à partir de la position 5
	// à partir de la position 8, pas de caractère => retourne la valeur std::string::npos
	cout << strCpp.find('o', 8) << " " << string::npos << endl;

	// -> rfind
	// idem find, mais on commence à partir de la fin de la chaine et on va vers le debut
	cout << strCpp.rfind('o') << endl;


	// Iterateur = > objet qui permet de parcourir la chaine(un peu comme un pointeur)
	// begin => retourne un iterateur sur le début de la chaine
	// end => retourne un iterateur sur la fin de la chaine
	for (string::iterator it = strCpp.begin(); it != strCpp.end(); it++) {	  // it++ permet de passer au caractère suivant
		cout << *it << endl;	// *it permet d'obtenir le caractère "pointer" par l'itérateur
		//	*it = 'a';			// on a accés en lecture et en écriture
	}
	cout << strCpp << endl;

	// rbegin et rend idem  mais l'itérateur part de la fin et va vers le début de la chaine
	for (auto it = strCpp.rbegin(); it != strCpp.rend(); it++) {	// it++ permet de passer au caractère précédent
		cout << *it << endl;
	}

	// en C++11 
	// Parcourir une chaine complétement
	for (auto chr : strCpp) {
		cout << chr << endl;
	}

	// Convertion string en chaine C (const *char)	-> c_str
	cout << strCpp.c_str() << endl;

	// Efface les caractères contenus dans la chaine
	strCpp.clear();	
	// empty => retourne true si la chaine est vide
	cout << strCpp.empty() << endl;

	cout << inverser("bonjour") << endl;
	cout << palindrome("bonjour") << endl;
	cout << palindrome("sos") << endl;
	return 0;
}

// Définition des fonctions -> déplacer dans un fichier sources .cpp
//double multiplication(double v1, double v2) {
//	return v1 * v2;
//}

//void afficher(double v) {
//	cout << "valeur=" << v << endl;
//	// return;
//}

