#include "MesFonctions.h"
#include <iostream>

using namespace std;

// Fichier .cpp  -> contient les D�finitions des fonctions
double multiplication(double v1, double v2) {
	return v1 * v2;
}

void afficher(double v) {
	cout << "valeur=" << v << endl;
	// return;
}

// Maximum
// �crire une fonction maximum qui prends en param�tre 2 nombres et elle retourne le maximum
// Saisir 2 nombres et afficher le maximum entre ces 2 nombres
int maximum(int va, int vb)
{
	return va > vb ? va : vb;

	//if (va > vb) {
	//	return va;
	//}
	//else {
	//	return vb;
	//}
}


// Fonction Paire
// �crire une fonction even qui prend un entier en param�tre
// Elle retourne vrai si il est paire
bool even(int v)
{
	return (v & 1) == 0;

	//return v % 2 == 0;

	// return v % 2 == 0 ? true : false;

	//if (v % 2 == 0) {
	//	return true;
	//}
	//else {
	//	return false;
	//}
}

// Passage de param�tres par valeur
// La valeur du param�tre est copi�e et une modification sur la copie n�entra�ne pas la modification de l�original
void testParamValeur(int val)
{
	cout << val << endl;
	val = 100;
	cout << val << endl;
}

// En utilisant le passage de param�tres par adresse, on modifie r�ellement la variable qui est pass�e en param�tre
void testParamAdresse(int* v1)
{
	cout << *v1 << endl;
	*v1 = 100;
	cout << *v1 << endl;
}

void afficherTab(int* tab, int size) {
	cout << "[\t";
	for (int i = 0; i < size; i++) {
		cout << tab[i] << "\t";
	}
	cout << "]" << endl;
}

// Passage de param�tre par r�f�rence
// Comme avec le passage de param�tres par adresse, on modifie r�ellement la variable qui est pass�e en param�tre
void testParamReference(int& val)
{
	cout << val << endl;
	val = 300;
	cout << val << endl;
}

// avec une r�f�rence constante, la valeur du parm�tre ne peut pas 
void testParamReferenceConst(const int& val)
{
	cout << val << endl;
	// val = 400;
}

// Les param�tres par d�faut sont uniquement d�fini dans la d�claration de la fonction (.h)
void testParamDefault(int i, const bool& b, char c)
{
	cout << i << " " << b << " " << c << endl;
}

inline int doubler(double a)
{
		return 2.0 * a;
}

// Surcharge de fonction
// Plusieurs fonctions peuvent avoir le m�me nom
// Ces arguments doivent �tre diff�rents en nombre ou/et en type pour qu'il n'y ai pas d'ambiguit� pour le compilateur
int somme(int a, int b)
{
	cout << "2 entiers" << endl;
	return a + b;
}

double somme(double d1, double d2)
{
	cout << "2 doubles" << endl;
	return d1 + d2;
}

double somme(int i, double d)
{
	cout << "1 entier 1 double" << endl;
	return i + d;
}

int somme(int a, int b, int c)
{
	cout << "3 entiers" << endl;
	return  a + b + c;
}

int somme(int* v, int i)
{
	cout << "1 pointeur et 1entiers" << endl;
	return  *v + i;
}
double somme(double& d, int i)
{
	cout << "une r�f�rence et une entier" << endl;
	return  d + i;
}

double somme(const double& d, int i)
{
	cout << "une r�f�rence constante et une entier" << endl;
	return  d + i;
}

// Fonction recursive
int factorial(int n) // factoriel= 1* 2* � n
{
	if (n <= 1)// condition de sortie
	{
		return 1;
	}
	else
	{
		return factorial(n - 1) * n;
	}
}

// Pointeur de fonction
double calcul(double a, double b, ptrf f)
{
	// return (*f)(a, b);
	return f(a, b);
}

// Classe de m�morisation static
void testMemStatic()
{
	// Entre 2 ex�cutions i conserve sa valeur
	static int i = 100;	// par d�faut initialis� � 0	
	i++;				// i n'est visible que dans la fonction
	cout << i << endl;
}

// Classe de m�morisation extern
void testMemExtern()
{
	cout << TEST_VAR_EXTERN << endl;
	//cout << pi << endl;
}

// /!\ une fonction ne doit pas retourner une r�f�rence ou un pointeur sur une variable locale � une fonction
string* testRetour(int n)
{
	//string tmp = string(n, 'a');		// variable locale ==> erreur
	string* tmp = new string(n, 'a'); // allocation dynamique avec un pointeur ou une r�f�rence
	return tmp;
}

// Nombre d'arguments variable
// en C et C++98
double moyenne(int nbarg, ...)
{
	va_list vl;
	va_start(vl, nbarg);	// initialise la liste des arguments variables. On passe en param�tre le nom du dernier param�tre nomm�
	double somme = 0;
	for (int i = 0; i < nbarg; i++) {
		somme += va_arg(vl, int);	// va_arg permet de recup�rer les valeurs des arguments dans le m�me ordre que celui transmis � la fonction
	}								// On passe en param�tre le type de l�argument
	va_end(vl);	// nettoyage des arguments
	return nbarg == 0 ? 0.0 : somme / nbarg;
}

// C++11 => initializer_list
double moyenneCpp11(initializer_list<int> valeurs)
{
	double somme = 0.0;
	for (auto v : valeurs) {
		somme += v;
	}
	return valeurs.size() == 0 ? 0.0 : somme / valeurs.size();
}

int* createTab(int size, int initValeur)
{
	int* tab = new int[size];
	for (int i = 0; i < size; i++) {
		tab[i] = initValeur;
	}
	return tab;
}

// Pointeur de pointeur
void createTab2(int** tab, int size, int initValeur)
{
	*tab = new int[size];
	for (int i = 0; i < size; i++) {
		(*tab)[i] = initValeur;
	}
}

int* saisieTab(int& size)
{
	cout << "Nombre d'element du tableau" << endl;
	cin >> size;
	int* tab = new int[size];
	for (int i = 0; i < size; i++) {
		cout << "Tab[" << i << "]=";
		cin >> tab[i];
	}
	return tab;
}

void calculTab(int* tab, int size, int& maximum, double& moyenne)
{
	maximum = tab[0];
	double somme = 0.0;
	for (int i = 0; i < size; i++) {
		somme += tab[i];
		if (tab[i] > maximum) {
			maximum = tab[i];
		}
	}
	moyenne = somme / size;
}


void afficherMenu() {
	cout << "1 - Saisir le tableau" << endl;
	cout << "2 - Afficher le tableau" << endl;
	cout << "3 - Afficher le maximum et la moyenne" << endl;
	cout << "0 - Quitter" << endl;
}

void menu() {
	int* t = nullptr;
	int sizeT = 0;

	int choix = 0;
	afficherMenu();
	do {
		cout << "choix=" << endl;
		cin >> choix;
		switch (choix) {
		case 1:
			delete[] t;
			t = saisieTab(sizeT);
			break;
		case 2:
			if (t != nullptr) {
				afficherTab(t, sizeT);
			}
			break;
		case 3:
			if (t != nullptr) {
				int max;
				double moy;
				calculTab(t, sizeT, max, moy);
				cout << "Maximum=" << max << " Moyenne=" << moy << endl;
			}
			break;
		case 0:
			delete[] t;
			cout << "Au revoir!" << endl;
			break;
		default:
			afficherMenu();
		}
	} while (choix != 0);

}

std::string inverser(const std::string& str)
{
	string tmp = "";
	for (auto it = str.rbegin(); it != str.rend(); it++) {
		tmp.push_back(*it);
	}
	return tmp;
}

bool palindrome(const std::string& str)
{
	return str == inverser(str);
}
