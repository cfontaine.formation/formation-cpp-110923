// Fichier .h -> contient la d�claration des fonctions

#pragma once // Indique au compilateur de n'int�grer le fichier d�en-t�te qu�une seule fois, lors de la compilation d�un fichier de code source
// ou
// #ifndef MES_FONCTIONS_H
// #define MES_FONCTIONS_H

#include<string>
#include<cstdarg>
#include<initializer_list>

// d�claration d'une variable qui a �t� d�finie dans main.cpp. Il n' y a pas d'allocation m�moire
extern const int TEST_VAR_EXTERN;

// extern double pi;	// pi est static dans 04-fonctions.cpp, on ne peut l'utiliser que dans ce fichier;

// D�claration d'une fonction
double multiplication(double, double);

// D�claration d'une fonction sans retour -> void
void afficher(double);

// Exercice fonction maximum
int maximum(int, int);

// Exercice fonction pair
bool even(int);

// Passage de param�tre par valeur
void testParamValeur(int);

// Passage de param�tre par adresse
// En utilisant le passage de param�tres par adresse, on modifie r�ellement la variable qui est pass�e en param�tre
void testParamAdresse(int*);

// Passage d'un tableau, comme param�tre d'une fonction
// On ne peut pas passer un tableau par valeur uniquement par adresse
//void afficherTab(int tab[], int size);
void afficherTab(int*, int);

// Passage de param�tre par r�f�rence
// Comme avec le passage de param�tres par adresse, on modifie r�ellement la variable qui est pass�e en param�tre
void testParamReference(int&);

// Si la r�f�rence est constante, on a le m�me r�sultat qu'un passage par valeur mais sans copie 
void testParamReferenceConst(const int&);

// On peut d�finir des valeurs par d�faut pour les param�tres. Ils doivent se trouver en fin de liste des arguments
// On place les valeurs par d�faut dans la d�claration des fonctions
void testParamDefault(int i, const bool& b = true, char c = 'a');

// Pour les fonctions inline, on place le code de la fonction dans le fichier.h
inline int doubler(double a);

// Surcharge de fonction
int somme(int, int);
double somme(double, double);
double somme(int, double);
int somme(int, int, int);
int somme(int*, int);
double somme(double&, int);
double somme(const double&, int);
//int somme(const int&, int); // ambigue -> int somme(int, int)

// R�cursivit�
int factorial(int n);

// Pointeur de fonction
// typedef permet de d�finir des synonymes de types -> typedef type synonyme_type;
typedef double (*ptrf)(double, double);
double calcul(double, double, ptrf f);

// Test de classe de m�morisation
void testMemStatic();
void testMemExtern();

// ERREUR => fonctions qui retournent une r�f�rence ou un pointeur sur une variable locale
std::string* testRetour(int);

// Fonction � nombre variable de param�tre
// h�rit� du C
double moyenne(int, ...);
// en C++ -> initializer_list
double moyenneCpp11(std::initializer_list<int>);

// Pointeur de pointeur
int* createTab(int size, int initValeur);
void createTab2(int** tab, int size, int initValeur);

// Exercice Tableau
int* saisieTab(int& size);

void afficherMenu();

void menu();

void calculTab(int* tab, int size, int& maximum, double& moyenne);

// Exercice Chaine de caract�re

std::string inverser(const std::string&);

bool palindrome(const std::string&);
//#endif