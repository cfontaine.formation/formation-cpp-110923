#pragma once
#include<string>

struct Contact
{
	// champs
	std::string prenom;
	std::string nom;
	std::string email;
	mutable int age; // mutable -> pour une strucutre constante, on peut modifier se champs

	// Fonctions en C++
	void afficher();
};

