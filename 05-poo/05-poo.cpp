#include <iostream>
#include"Voiture.h"
#include"CompteBancaire.h"
#include"Point.h"
#include"VoiturePrioritaire.h"
#include "CompteEpargne.h"

using namespace std;

int main()
{
	// Accès une variable de classe
	cout << "nombre voiture=" << Voiture::getCptVoiture() << endl; //Voiture::cptVoiture

	// Appel méthode de classe
	Voiture::testMethodeClasse();

	// Instanciation statique ou automatique
	Voiture v1;
	cout << "nombre voiture=" << Voiture::getCptVoiture() << endl;
	//v1.vitesse = 10;
	cout << v1.getVitesse() << endl; //v1.vitesse

	// Appel d'une méthode d'instance
	v1.accelerer(30);
	v1.afficher();
	v1.freiner(15);
	v1.afficher();
	cout << v1.estArreter() << endl;
	v1.arreter();
	cout << v1.estArreter() << endl;

	// Instanciation dynamique
	Voiture* v2 = new Voiture;
	cout << "nombre voiture=" << Voiture::getCptVoiture() << endl;
	//v2->vitesse = 15;
	cout << v2->getVitesse() << endl;
	v2->afficher();

	Voiture v3("opel", "gris", "AZ-3456-FR");
	v3.afficher();

	Voiture* v4 = new Voiture("fiat", "jaune", "AZ-3456-FR");
	v4->afficher();

	// Constructeur par copie ou recopie
	Voiture v5(v3); // ou Voiture v5=v3;
	v5.afficher();

	delete v2;
	delete v4;

	// Méthode de classe
	cout << Voiture::egaliteVitesse(v1, v3) << endl;

	// Objet Constant
	const Voiture vCst("Honda", "vert", "ZS-1234-TY");
	cout << vCst.getVitesse() << endl;
	vCst.afficher();

	// Agrégation
	Personne per1("John", "Doe");
	Voiture* v6 = new Voiture("Subaru", "rouge", "az-1234-rf", per1);
	v6->afficher();
	
	delete v6;
	per1.afficher();

	// Héritage
	VoiturePrioritaire vp1;
	vp1.accelerer(30);
	vp1.afficher();
	vp1.alumerGyro();
	cout << vp1.isGyro() << endl;

	VoiturePrioritaire vp2("Citroen", "noir", "az-1234-TY", true);
	vp2.afficher();
	cout << vp2.isGyro() << endl;

	// Exercice: Compte Bancaire
	CompteBancaire cb1("John Doe", 100.0);

	cb1.afficher();
	cb1.crediter(30.0);
	cb1.debiter(50.5);
	cb1.afficher();
	cout << cb1.getSolde() << " " << cb1.getTitulaire() << endl;

	CompteBancaire cb2("Jane Doe", 200.0);
	cb2.afficher();

	// Exercice Hritage: Compte Epargne
	CompteEpargne ce1(120.0, "Jane Doe", 1.5);
	ce1.afficher();
	ce1.calculInterets();
	ce1.afficher();

	// Exercice Point
	Point a;
	a.afficher();
	Point b(1, 1);
	b.afficher();
	b.deplacer(2, 1);
	b.afficher();
	cout << "Norme=" << b.norme() << endl;
	cout << "Distance=" << Point::distance(a, b) << endl;
}

