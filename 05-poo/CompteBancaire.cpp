#include "CompteBancaire.h"
#include <iostream>

using namespace std;

int CompteBancaire::cpt = 0;

void CompteBancaire::crediter(double valeur)
{
	if (valeur > 0.0) {
		solde += valeur;
	}
}

void CompteBancaire::debiter(double valeur)
{
	if (valeur > 0.0) {
		solde -= valeur;
	}
}

bool CompteBancaire::estPositif() const
{
	return solde > 0.0;
}

void CompteBancaire::afficher() const
{
	cout << "__________________________" << endl;
	cout << "Solde= " << solde << endl;
	cout << "Iban= " << iban << endl;
	cout << "Titulaire= " << titulaire << endl;
	cout << "__________________________" << endl;
}


