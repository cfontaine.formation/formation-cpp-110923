#pragma once
#include<string>
class CompteBancaire
{
protected:
	double solde;
private:
	std::string iban;
	std::string titulaire;

	static int cpt;

	public:
	CompteBancaire(std::string titulaire) : solde(0.0),  titulaire(titulaire) {
		cpt++;
		iban = "fr-6259-0000-" + std::to_string(cpt);
	}

	CompteBancaire(std::string titulaire,double solde) : solde(solde),  titulaire(titulaire) {
		cpt++;
		iban = "fr-6259-0000-" + std::to_string(cpt);
	}

	// Pour supprimer le constructeur
	CompteBancaire(const CompteBancaire& cb) = delete;

	double getSolde() const {
		return solde;
	}

	std::string getIban() const {
		return iban;
	}

	std::string getTitulaire() const {
		return titulaire;
	}

	void setTitulaire(const std::string &titulaire) {
		this->titulaire = titulaire;
	}

	void crediter(double valeur);
	void debiter(double valeur);
	bool estPositif() const;
	virtual void afficher() const;
};

