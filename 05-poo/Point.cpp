#include "Point.h"
#include <iostream>
#include <cmath>

using namespace std;

void Point::afficher() const
{
	cout << "(" << x << "," << y << ")"<<endl;
}

void Point::deplacer(int tx, int ty)
{
	x += tx;
	y += ty;
}

double Point::norme() const
{
	return std::sqrt(x * x + y * y);
}

double Point::distance(const Point& p1, const Point& p2)
{
	return std::sqrt(std::pow((p2.x - p1.x), 2) + std::pow((p2.y - p1.y), 2));
}


