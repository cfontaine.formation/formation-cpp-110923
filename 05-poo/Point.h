#pragma once
class Point
{
	int x;
	int y;

public:
	Point(): x(0),y(0) {

	}

	Point(int x, int y): x(x),y(y){
	}

	int getX() const {
		return x;
	}

	int getY() const {
		return y;
	}

	void setX(int x) {
		this->x = x;
	}

	void setY(int y) {
		this->y = y;
	}

	void afficher() const;
	void deplacer(int tx, int ty);
	double norme() const;

	static double distance(const Point& p1, const Point& p2);
};

