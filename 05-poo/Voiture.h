#pragma once
#include"Personne.h"
#include "Moteur.h"
#include<string>
#include <iostream>

class Voiture
{
	// class -> par defaut private

	// Variable d'instance
	std::string marque = "ford"; // en C++11, on peut initialiser une variable d'instance
	std::string couleur;
	std::string plaqueIma;		// en C++11, on peut initialiser une variable d'instance
protected:
	int vitesse = 0;
private:
	int compteurKm = 20;		// en C++11, on peut initialiser une variable d'instance

	// Variable de classe
	static int cptVoiture;

	// Agr�gation
	Personne* proprietaire;

	// Composition
	// Moteur moteur;
	// ou
	Moteur* moteur;

public:
	// Constructeurs

	// Voiture();
	// Voiture(std::string m, std::string c, std::string pi)

	// Constructeur d�faut
	//Voiture() {
	//	marque = "Ford";
	//	couleur = "Bleu";
	//	plaqueIma = "AZ-3456-RT";
	//	vitesse = 0;
	//	compteurKm = 10;
	// proprietaire = nullptr;
	// moteur = new Moteur;
	//};

	// En C++11, default -> permet de cr�er un constructeur par d�faut
	//Voiture() = default;


	// Liste d'initialisation
	Voiture(std::string marque, std::string couleur, std::string plaqueIma)
		: marque(marque), couleur(couleur), plaqueIma(plaqueIma), vitesse(0), compteurKm(10), proprietaire(nullptr) {
		cptVoiture++;
		moteur = new Moteur;
	}

	Voiture(std::string marque, std::string couleur, std::string plaqueIma, int vitesse)
		: marque(marque), couleur(couleur), plaqueIma(plaqueIma), vitesse(vitesse), compteurKm(10), proprietaire(nullptr) {
		cptVoiture++;
		moteur = new Moteur;
	}

	Voiture(std::string marque, std::string couleur, std::string plaqueIma, Personne& proprietaire)
		: marque(marque), couleur(couleur), plaqueIma(plaqueIma), vitesse(0), compteurKm(10), proprietaire(&proprietaire) {
		cptVoiture++;
		moteur = new Moteur;
	}

	// Constructeur par copie
	// Il n'est pas n�cessaire de faire un constructeur par copie, si l'on n'a pas d'attribut allou� dynamiquement (pointeur)
	Voiture(const Voiture& v); // ou Voiture(Voiture& v)

	// Supprimer le constucteur par copie
	// En C++98
	// - uniquement le d�clarer et ne pas donner sa d�finition
	//  Voiture(const Voiture& v);

	// - Le rendre priv�e
	// private:
	//	Voiture(const Voiture& v);
	// public:

	// En C++ 11 -> delete
	// Voiture(const Voiture& v) = delete;

	// En C++11 Constructeur d�l�gu�
	Voiture() : Voiture("Toyota", "Blanc", "ZI-1234-TY") {
		std::cout << "Constructeur d�l�gu� par d�faut" << std::endl;
	}

	// Destructeur
	~Voiture();

	// Getter/Setter
	// Les m�thodes qui sont d�finie dans .h est implicitement inline
	// const-> Un m�thode peut �tre constante si elle ne modifie pas l'�tat de l'objet
	// Les m�thodes constantes sont les seules que l'on peut utiliser, si l'objet est constant
	std::string getMarque() const {
		return marque;
	}

	std::string getCouleur() const {
		return couleur;
	}

	void setCouleur(const std::string& couleur) {
		this->couleur = couleur;	// this permet de lever l'ambiguit� entre une variable d'instance et le param�tre
	}

	std::string getPlaqueIma() const {
		return plaqueIma;
	}

	int getVitesse() const {
		return vitesse;
	}

	int getCompteurKm() const {
		return compteurKm;
	}

	static int getCptVoiture() {	// -> Une m�thode de classe ne peut pas �tre const
		return cptVoiture;
	}

	Personne* getProprietaire() const {
		return proprietaire;
	}

	void setProprietaire(Personne* propritaire) {
		this->proprietaire = proprietaire;
	}

	Moteur* getMoteur() const
	{
		return moteur;
	}

	// M�thode d'instance
	void accelerer(int vAcc);
	void freiner(int vFrn);
	void arreter();
	bool estArreter() const;
	virtual void afficher() const;

	// M�thode de classe
	static void testMethodeClasse();
	static bool egaliteVitesse(const Voiture& v1, const Voiture& v2);
};

