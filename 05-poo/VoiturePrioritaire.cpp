#include "VoiturePrioritaire.h"
using namespace std;
void VoiturePrioritaire::alumerGyro()
{
	gyro = true;
}

void VoiturePrioritaire::eteindreGyro()
{
	gyro = false;
}

void VoiturePrioritaire::afficher() const
{
	Voiture::afficher();
	cout << (gyro ? "gyro allumer" : "gyro �teint") << endl;
}
