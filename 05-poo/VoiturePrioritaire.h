#pragma once
#include "Voiture.h"
#include <iostream>

class VoiturePrioritaire : public Voiture
{
	bool gyro;

public:

	VoiturePrioritaire() :gyro(false) { // appel implicite du constructeur par d�faut de la classe m�re
		std::cout << "Constructeur par d�faut de la classe enfant" << std::endl;
	}

	// appel explicite du constrructeur 3 param�tres de la classe m�re Voiture
	VoiturePrioritaire(std::string marque, std::string couleur, std::string plaqueIma, bool gyro)
		: Voiture(marque, couleur, plaqueIma), gyro(gyro) {
		std::cout << "Constructeur 3 param�tres de la classe enfant" << std::endl;
	}

	bool isGyro() const {
		return gyro;
	}

	void alumerGyro();
	void eteindreGyro();
	void afficher() const;
};

