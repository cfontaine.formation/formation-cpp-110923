#include <iostream>
#include"Animal.h"
#include"Chien.h"
#include "Animalerie.h"
#include "Chat.h"
using namespace std;

int main()
{
	// La classe Animal est abstraite, elle ne peut plus �tre instensi� 
	//Animal* a1 = new Animal(2, 4000);
	//a1->afficher();
	//a1->emmetreSon();

	Chien* ch1 = new Chien("Rolo", 4, 8000);
	ch1->afficher();
	ch1->emmetreSon();
	cout << ch1->getNom() << endl;

	Animal* a2 = new Chien("Laika", 2, 3000);
	a2->emmetreSon();	 // virtual appel de la m�thode emmetreSon de Chien

	//Chien* ch2 = (Chien*) a2; // cast C
	Chien* ch2 = dynamic_cast<Chien*>(a2);
	if (ch2 != nullptr) {
		cout << ch2->getNom() << endl;
	}
	else {
		cout << "erreur" << endl;
	}
	Animal* a3 = new Chat(3, 5000);

	//Chien* ch3 = dynamic_cast<Chien*>(a1); // retourner 0 ou nullptr et exception bad_cast
	//if (ch3) {
	//	ch3->emmetreSon();
	//	cout << ch3->getNom() << endl;
	//}

	Animalerie an;
	an.ajouter(*ch1);
	an.ajouter(*ch2);
	an.ajouter(*a3);
	an.ecouter();

	//delete a1;
	delete ch1;
	delete a2;
	delete a3;

}
