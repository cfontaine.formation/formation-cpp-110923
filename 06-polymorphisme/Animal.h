#pragma once
class Animal
{
	int age;
	int poid;

public:
	Animal(): age(0),poid(0) {
	
	}

	Animal(int age, int poid) :age(age), poid(poid) {

	}

	// virtual ~Animal() = 0; // si on veut cr�er, une classe abstraite sans avoir de m�thode virtuelle pure dans la classe
							  // |-> on en cr�e une avec le destructeur

	int getAge() const {
		return age;
	}

	void setAge(int age) {
		this->age = age;
	}

	int getPoid() const {
		return poid;
	}

	void setPoid(int poid) {
		this->poid = poid;
	}

	virtual void afficher() const;

	// M�thode abstraite -> M�thode virtuelle pure, la classe devient abstraite
	virtual void emmetreSon() =0;
};

