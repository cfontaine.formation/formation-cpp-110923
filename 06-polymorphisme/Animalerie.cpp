#include "Animalerie.h"

void Animalerie::ajouter(Animal& animal){
	if (nbOccupeBox < 10) {
		box[nbOccupeBox] = &animal;
		nbOccupeBox++;
	}
}

void Animalerie::ecouter()
{
	for (int i = 0; i < nbOccupeBox; i++) {
		box[i]->emmetreSon();
	}
}
