#pragma once
#include "Animal.h"

class Animalerie
{
	Animal* box[10];
	int nbOccupeBox;

public:
	Animalerie() {
		for (int i = 0; i < 10; i++) {
			box[i] = nullptr;
		}
		nbOccupeBox = 0;
	}

	void ajouter(Animal& animal);
	void ecouter();
};

