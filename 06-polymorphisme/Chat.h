#pragma once
#include "Animal.h"
class Chat : public Animal
{

	int nbVie;

public:
	Chat( int age, int poid) :Animal(age, poid), nbVie(9) {

	}

	int getNbVie()const {
		return nbVie;
	}

	void afficher() const;

	void emmetreSon() ;
};

