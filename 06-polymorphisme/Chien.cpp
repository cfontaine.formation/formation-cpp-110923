#include "Chien.h"
#include <iostream>

void Chien::afficher() const
{
	Animal::afficher();
	std::cout << nom << std::endl;
}

void Chien::emmetreSon() 
{
	std::cout << nom << " aboie" << std::endl;
}
