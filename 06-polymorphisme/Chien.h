#pragma once
#include "Animal.h"
#include <string>
class Chien :   public Animal
{
	std::string nom;

public:
	Chien(const std::string& nom, int age, int poid) :Animal(age, poid), nom(nom) {

	}

	std::string getNom()const {
		return nom;
	}

	void afficher() const;

	void emmetreSon() ;

};

