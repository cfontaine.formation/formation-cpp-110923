#include <iostream>
#include "Terrain.h"
#include "Rectangle.h"
#include "Cercle.h"
#include "TriangleRectangle.h"
using namespace std;

int main()
{
	Terrain terrain;
	Rectangle r1 = Rectangle(Couleur::BLEU, 1.0, 1.0);
	Rectangle r2 = Rectangle(Couleur::BLEU, 1.0, 1.0);
	Rectangle r3 = Rectangle(Couleur::BLEU, 1.0, 1.0);
	Cercle c1 = Cercle(Couleur::ROUGE, 1.0);
	Cercle c2 = Cercle(Couleur::ROUGE, 1.0);
	TriangleRectangle tr1 = TriangleRectangle(Couleur::VERT, 1.0, 1.0);
	Rectangle r4 = Rectangle(Couleur::ORANGE, 1.0, 1.0);
	TriangleRectangle tr2 = TriangleRectangle(Couleur::ORANGE, 1.0, 1.0);
	terrain.ajoutForme(r1);
	terrain.ajoutForme(r2);
	terrain.ajoutForme(r3);
	terrain.ajoutForme(c1);
	terrain.ajoutForme(c2);
	terrain.ajoutForme(tr1);
	terrain.ajoutForme(r4);
	terrain.ajoutForme(tr2);

	cout << "Surface total=" << terrain.surfaceTotal() << endl;
	cout << "----------------------------------------------" << endl;
	cout << "Surface bleu=" << terrain.surfaceTotal(Couleur::BLEU) << endl;
	cout << "Surface vert=" << terrain.surfaceTotal(Couleur::VERT) << endl;
	cout << "Surface rouge=" << terrain.surfaceTotal(Couleur::ROUGE) << endl;
	cout << "Surface orange=" << terrain.surfaceTotal(Couleur::ORANGE) << endl;

}

// Exécuter le programme : Ctrl+F5 ou menu Déboguer > Exécuter sans débogage
// Déboguer le programme : F5 ou menu Déboguer > Démarrer le débogage

// Astuces pour bien démarrer : 
//   1. Utilisez la fenêtre Explorateur de solutions pour ajouter des fichiers et les gérer.
//   2. Utilisez la fenêtre Team Explorer pour vous connecter au contrôle de code source.
//   3. Utilisez la fenêtre Sortie pour voir la sortie de la génération et d'autres messages.
//   4. Utilisez la fenêtre Liste d'erreurs pour voir les erreurs.
//   5. Accédez à Projet > Ajouter un nouvel élément pour créer des fichiers de code, ou à Projet > Ajouter un élément existant pour ajouter des fichiers de code existants au projet.
//   6. Pour rouvrir ce projet plus tard, accédez à Fichier > Ouvrir > Projet et sélectionnez le fichier .sln.
