#include <iostream>
#include "Maclasse.h"
#include "ClasseAmie.h"

// fonctionAmie peut acc�der  la variable d'instance val de la classe MaClasse car elle est d�clar�e amie
void fonctionAmie(Maclasse& m) {
	m.val = 123;
}

int main()
{
	// Fonction amie
	Maclasse mc(42);
	mc.afficher();
	fonctionAmie(mc);
	mc.afficher();

	// Classe amie
	ClasseAmie ca(mc);
	ca.traitement();
	mc.afficher();
}
