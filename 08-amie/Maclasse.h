#pragma once
class Maclasse
{
	int val;

public:
	Maclasse(int val) :val(val) {

	}

	int getVal() const {
		return val;
	}

	void afficher();

	// Fonction Amie
	friend void fonctionAmie(Maclasse&);

	// Classe Amie
	friend class ClasseAmie;
};

