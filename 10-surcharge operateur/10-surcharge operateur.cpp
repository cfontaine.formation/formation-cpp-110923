
#include <iostream>
#include "Point.h"
#include "Fraction.h"

using namespace std;

int main()
{
	Point a(2, 2);
	Point b(2, -4);

	// opérateur -
	Point c = -a;
	cout << a << " " << c << endl;

	// Opération ++
	Point d = ++a;
	cout << a << " " << d << endl;

	Point e = a++;
	cout << a << " " << e << endl;

	// Operateur []
	cout << a[0] << a[1] << endl;

	// Operateur +
	Point f = a + b;
	cout << f << endl;

	// Operateur *
	Point g = a * 2;
	cout << g << endl;

	// Operateur == !=
	if (a != b) {
		cout << "!=" << endl;
	}

	// Exercice Fraction
	Fraction f1(4, 8);
	Fraction f2(1, 2);
	Fraction r = f1 + f2;
	std::cout << r << std::endl;
	r = f1 * f2;
	std::cout << r << std::endl;
	r = f1 * 2;
	std::cout << r << std::endl;
	cout << (f1 == f2) << endl;
}
