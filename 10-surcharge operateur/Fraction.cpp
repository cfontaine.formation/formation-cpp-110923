#include "Fraction.h"
#include <ostream>
#include <string>

Fraction operator+(const Fraction& f1, const Fraction& f2)
{
	int n = f1.numerateur * f2.denominateur + f2.numerateur * f1.denominateur;
	int d = f1.denominateur * f2.denominateur;
	return Fraction(n, d);
}

Fraction operator*(const Fraction& f1, const Fraction& f2)
{
	int n = f1.numerateur * f2.numerateur;
	int d = f1.denominateur * f2.denominateur;
	return Fraction(n, d);
}

Fraction operator*(const Fraction& f1, const int& scal)
{
	int n = f1.numerateur * scal;
	int d = f1.denominateur;
	return Fraction(n, d);
}

bool operator==(const Fraction& f1, const Fraction& f2)
{
	return f1.valeur() == f2.valeur(); // interval
}

bool operator!=(const Fraction& f1, const Fraction& f2)
{
	return !(f1 == f2);
}

std::ostream& operator<<(std::ostream& os, const Fraction& f)
{

	return os << f.numerateur << (f.denominateur == 1 ? "" : "/" + std::to_string(f.denominateur));
}
