#include <iostream>
#include <cmath>
#include "Point.h"

using namespace std;

void Point::deplacer(int tx, int ty)
{
	x += tx;
	y += ty;
}

double Point::norme() const
{
	return std::sqrt(x * x + y * y);
}

double Point::distance(const Point& p1, const Point& p2)
{
	return std::sqrt(std::pow((p2.x - p1.x), 2) + std::pow((p2.y - p1.y), 2));
}

Point Point::operator-()
{
	return Point(-x, -y);
}

Point Point::operator++()
{
	x++;
	y++;
	return Point(x, y);
}

Point Point::operator++(int)
{
	Point tmp = *this;
	x++;
	y++;
	return tmp;
}

int& Point::operator[](int index)
{
	if (index == 0) {
		return x;
	}
	else if (index == 1) {
		return y;
	}
	else {
		throw std::out_of_range("indice suprieur  1 ou infrieur   0");
	}
}

Point operator+(const Point& p1, const Point& p2)
{
	return Point(p1.x + p2.x, p1.y + p2.y);
}

Point operator*(const Point& p, const int& scalaire)
{
	return Point(p.x * scalaire, p.y * scalaire);
}

bool operator==(const Point& p1, const Point& p2)
{
	return p1.x == p2.x && p1.y == p2.y;
}

bool operator!=(const Point& p1, const Point& p2)
{
	return p1.x != p2.x || p1.y != p2.y;
}

std::ostream& operator<<(std::ostream& os, const Point& p)
{
	return os << "(" << p.x << "," << p.y << ")";
}
