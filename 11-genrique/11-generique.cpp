#include <iostream>
#include <string>
#include "Pair.h"
using namespace std;

template<typename T> T somme(T a, T b)
{
	return a + b;
}

int main()
{
	// Fonction générique
	cout << somme<int>(2, 4) << endl;
	cout << somme(1L, 2L) << endl;
	cout << somme(1.5, 2.5) << endl;
	cout << somme(string("aze"), string("rty")) << endl;

	// Classe générique => un seul fichier .h
	Pair<int> p(12, 34);
	cout << p.minimum() << endl;
	Pair<string> ps("aze", "rty");
	cout << ps.minimum() << endl;

}
