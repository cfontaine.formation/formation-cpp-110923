#include <iostream>
#include <cmath>
#include "Point.h"
#include <stdexcept>

using namespace std;

void Point::afficher() const
{
    cout << "(" << x << "," << y << ")" << endl;
}

void Point::deplacer(const int& tx, const int& ty)
{
    x += tx;
    y += ty;
}

double Point::norme() const
{
    return sqrt(pow(x, 2) + pow(y, 2));
}

double Point::distance(const Point& p) const
{

    return sqrt(pow((p.x - x), 2) + pow((p.y - y), 2));
}

double Point::distance(const Point& p1, const Point& p2)
{
    return sqrt(pow((p2.x - p1.x), 2) + pow((p2.y - p1.y), 2));
}

Point Point::operator-()
{
    return Point(-x, -y);
}

Point Point::operator++()
{
    ++x;
    ++y;
    return Point(x, y);
}

Point Point::operator++(int)
{
    Point tmp = *this;
    x++;
    y++;
    return tmp;
}

int& Point::operator[](int index)
{
    if (index == 0) {
        return x;
    }
    else if (index == 1) {
        return y;
    }
    else {
        throw std::out_of_range("indice >1");
    }
}

Point operator+(const Point& p1, const Point& p2)
{
    return Point(p1.x + p2.x, p1.y + p2.y);
}

Point operator*(const Point& p1, int scalaire)
{
    return Point(p1.x * scalaire, p1.y * scalaire);
}

bool operator==(const Point& p1, const Point& p2)
{
    return p1.x == p2.x && p1.y == p2.y;
}

bool operator!=(const Point& p1, const Point& p2)
{
    return !(p1 == p2);
}

std::ostream& operator<<(std::ostream& os, const Point& p)
{
    return os << "(" << p.x << "," << p.y << ")";
}
