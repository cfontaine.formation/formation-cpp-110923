#pragma once
#include<ostream>

class Point
{
	int x;
	int y;

public:
	Point() : x(0), y(0) {};

	Point(const int& x, const int& y) : x(x), y(y) {};

	int getX() const
	{
		return x;
	}

	int getY() const
	{
		return y;
	}

	void setX(const int& x)
	{
		this->x = x;
	}

	void setY(const int& y)
	{
		this->y = y;
	}

	void afficher() const;

	void deplacer(const int& tx, const int& ty);

	double norme() const;

	double distance(const Point& p) const;

	static double distance(const Point& p1, const Point& p2);

	Point operator-();
	Point operator++(); // pré-incrémentation
	Point operator++(int);// post-incrémentation
	int& operator[](int index);


	friend Point operator+(const Point& p1, const Point& p2);
	friend Point operator*(const Point& p1, int scalaire);
	friend bool operator==(const Point& p1, const Point& p2);
	friend bool operator!=(const Point& p1, const Point& p2);
	friend std::ostream& operator<<(std::ostream& os, const Point& p);
};

