#include<iostream>
#include "Voiture.h"
using namespace std;

int Voiture::cptVoiture = 0;

void Voiture::accelerer(int vAcc)
{
	if (vAcc > 0)
	{
		this->vitesse += vAcc;
	}
}

bool Voiture::egaliterVitese(const Voiture& v1, const Voiture& v2)
{
	return v1.vitesse == v2.vitesse;
}


void Voiture::freiner(int vFrn)
{
	if (vFrn > 0)
	{
		vitesse -= vFrn;
	}
}

void Voiture::arreter()
{
	vitesse = 0;
}

bool Voiture::estArreter() const
{
	return vitesse == 0;
}

void Voiture::afficher() const
{
	cout << "[" << marque << " " << couleur << " " << plaqueIma << " " << vitesse << " " << compteurKm << " ]" << endl;
}
