#pragma once
#include <string>
class Voiture
{
	std::string marque;
	std::string couleur;
	std::string plaqueIma;

protected:
	int vitesse = 0;

private:
	int compteurKm = 10;


	static int cptVoiture;

public:

	Voiture() : Voiture("Opel", "Gris", "fg-1234-FR") {	};

	Voiture(std::string marque, std::string couleur, std::string plaqueIma) : marque(marque), couleur(couleur), plaqueIma(plaqueIma), vitesse(0), compteurKm(10)
	{
		cptVoiture++;
	};

	Voiture(std::string marque, std::string couleur, std::string plaqueIma, int compteurKm) : marque(marque), couleur(couleur), plaqueIma(plaqueIma), compteurKm(compteurKm)
	{
		cptVoiture++;
	};

	std::string getMarque() const
	{
		return marque;
	}

	std::string getCouleur() const
	{								
		return couleur;
	}

	void setCouleur(const std::string& couleur)
	{
		this->couleur = couleur; 
	}

	int getVitesse() const
	{
		return vitesse;
	}

	int getCompteurKm() const
	{
		return compteurKm;
	}

	static int getCptVoiture()
	{
		return cptVoiture;
	}

	void accelerer(int vAcc);
	void freiner(int vFrn);
	void arreter();
	bool estArreter() const;
	void afficher()  const;

	static bool egaliterVitese(const Voiture& v1, const Voiture& v2);
};

